const expect = require('chai').expect;
describe('Simple Math Test A', () => {
 it('should return 2', () => {
        expect(1 + 1).to.equal(2);
    });
 it('should return 4', () => {
        expect(2 + 2).to.equal(4);
    });
});